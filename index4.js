var firmata = require('firmata');
var http = require('http');
var fs = require('fs').readFileSync(__dirname + '/button.html');

console.log("Starting system...");

var board = new firmata.Board("/dev/ttyACM0", function() {
    try {
        console.log("Starting Arduino and activating Pins 2 and 13");
        board.pinMode(13, board.MODES.OUTPUT);
        board.pinMode(2, board.MODES.INPUT);
    }
    catch (err) {
        console.log("Something went wrong with Arduino connection!")
    }
    console.log("Firmware: " + board.firmware.name + " - " + board.firmware.version.major);
});    

var level = 0;

var plainHttpServer = http.createServer(function(request, response) {
    response.writeHead(200, {"Content-Type": "text/html"});
    response.end(fs);
})
.listen(8080);

var io = require('socket.io').listen(plainHttpServer);
io.set('origins', ['192.168.1.135:8080']);

io.sockets.on('connection', function(socket) {
    socket.emit('hello', 'Greet by socket.io');
    
    board.digitalRead(2, function(value) {
        if (value === 0) {
            console.log("LED OFF");
            board.digitalWrite(13, board.LOW);
            console.log("value = 0, LED shut down");
            level = 0;
        }
        else if (value === 1) {
            console.log("LED ON");
            board.digitalWrite(13, board.HIGH);
            console.log("value = 1, LED lit");
            level = 1;
        }
        else {
            console.log("Panic - abnormal value read from PIN 2!");
        }
        
        socket.emit("Level", value);
    });
    
    socket.on('hello', function(msg) {
        console.log('Received a hello event from ' + msg);
        
    });
    
    socket.on('message', function(msg) {
        console.log('Received a message event from ' + msg);
    });
});
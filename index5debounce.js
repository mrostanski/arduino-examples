var firmata = require('firmata');
var http = require('http');
var fs = require('fs').readFileSync(__dirname + '/buttondebounce.html');

console.log("Starting system...");

var board = new firmata.Board("/dev/ttyACM0", function() {
    try {
        console.log("Starting Arduino and activating Pins 2 and 13");
        board.pinMode(13, board.MODES.OUTPUT);
        board.pinMode(2, board.MODES.INPUT);
    }
    catch (err) {
        console.log("Something went wrong with Arduino connection!");
    }
    console.log("Firmware: " + board.firmware.name + " - " + board.firmware.version.major);
});    

board.on("ready", function() {

    var level = 0;

    var plainHttpServer = http.createServer(function(request, response) {
        response.writeHead(200, {"Content-Type": "text/html"});
        response.end(fs);
    }).listen(8080);

    var io = require('socket.io').listen(plainHttpServer);
    io.set('origins', ['192.168.1.135:8080']);

    io.sockets.on('connection', function(socket) {
        
//        setTimeout(readPin(),50);
        console.log("connection ON");
        
        socket.emit('hello', 'Hello, ' + socket.conn.remoteAddress + ' , Greetings by socket.io');
        
//        function readPin(){
        
        var timeout = false,
            sensitivity = 50,
            last_sent = null,
            last_value = null;
            board.digitalRead(2, function(value) {
                if (timeout !== false) {
                    clearTimeout(timeout);
                }
                timeout = setTimeout( function() {
                    console.log("Timeout set to false");
                    timeout = false;
                    if (last_value != last_sent) {
                        if (value == 0) {
                            console.log("LED OFF");
                            board.digitalWrite(13, board.LOW);
                            console.log("value = 0, LED shut down");
                            level = 0;
                        }
                        else if (value == 1) {
                            console.log("LED ON");
                            board.digitalWrite(13, board.HIGH);
                            console.log("value = 1, LED lit");
                            level = 1;
                        }
                        else {
                            console.log("Panic - abnormal value read from PIN 2!");
                            process.exit(-1);
                        }
                        
                        socket.emit("Level", value);
                        
                    }
                    
                    last_sent = last_value;
                }, sensitivity);
                
                last_value = value;
                
            });
        
            
  //      }
            
    
        socket.on('hello', function(msg) {
            console.log('Received a hello event from ' + socket.conn.remoteAddress + ', msg: '+ msg);
        
        });
    
        socket.on('message', function(msg) {
            console.log('Received a message event from ' + msg);
        });
    });

});
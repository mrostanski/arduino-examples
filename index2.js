var firmata = require('firmata');
var http = require('http');

console.log("Started.");

var board = new firmata.Board("/dev/ttyACM0", function() {
    console.log("Started Arduino...");
    console.log("Activating PIN 12");
    board.pinMode(12, board.MODES.OUTPUT);
    console.log("Activating PIN 13");
    board.pinMode(13, board.MODES.OUTPUT);
    
    http.createServer(function(request, response) {
        var parts = request.url.split("/");
        var operator = parseInt(parts[1],10);
        
        console.log("Connection!");
        console.log(request.headers);
        if (operator == 0) {
            console.log("Turning LED off");
            board.digitalWrite(13, board.LOW);
        }
        else if (operator == 1) {
            console.log("Turning LED on");
            board.digitalWrite(13, board.HIGH);
        }
        else if (operator == 2) {
            console.log("Turning LED off");
            board.digitalWrite(12, board.LOW);
        }
        else if (operator == 3) {
            console.log("Turning LED on");
            board.digitalWrite(12, board.HIGH);
        }
        else {
            console.log("Something is wrong!");
        }
        
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.write("Put following URL in order to light LED:\n http://192.168.1.135/1\n Press ENTER \n");
        response.end("The operator call was: " + operator);
        
    }).listen(8081, "192.168.1.135");
});

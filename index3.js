var firmata = require('firmata');
var http = require('http').createServer(handler);
var fs = require('fs');
var io = require('socket.io').listen(http);


console.log("Started.");

function handler(request, response) {
    fs.readFile(__dirname + "/arduino.html", function (err, data) {
        if (err) {
            response.writeHead(500, {"Content-Type": "text/plain"});
            return response.end("No html file found!");
        }
        response.writeHead(200);
        response.end(data);
    });
}

http.listen(8081);
console.log("System ready.");

var board = new firmata.Board("/dev/ttyACM0", function() {
    console.log("Started Arduino...");
    console.log("Activating PIN 13");
    board.pinMode(13, board.MODES.OUTPUT);
});    

io.sockets.on("connection", function(socket) {
    socket.emit("New client connected", "Event sent.");
    socket.on("ArduinoShow", function(order) {
        if (order == "1") {
            board.digitalWrite(13, board.HIGH);
        }
        else if (order == "0") {
            board.digitalWrite(13, board.LOW);
        }
        else {
            console.log("Unknown order");
        }
    });
});

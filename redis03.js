// primer z zapisom polja (Array)

var http = require("http").createServer(handler); // handler za delo z aplikacijo
var io = require("socket.io").listen(http); // socket.io za trajno povezavo med strež. in klient.
var fs = require("fs"); // spremenljivka za "file system", t.j. fs

var redis  = require("redis");
var clientRedis = redis.createClient();

function handler(req, res) {
    fs.readFile(__dirname + "/redis03.html",
    function (err, data){
        if (err) {
            res.writeHead(500, {"Content-Type": "text/plain"});
            return res.end("Napaka pri nalaganju html strani");
        }
    res.writeHead(200);
    res.end(data);
    })
}

http.listen(8080); // določimo na katerih vratih bomo poslušali

console.log("Zagon sistema"); // v konzolo zapišemo sporočilo (v terminal)

io.sockets.on("connection", function(socket) {

	socket.on("srvPisiVbazo", function(podatek){
		console.log("Pišemo podatek: " + podatek);
		clientRedis.rpush("zapis", podatek); // "zapis" je ključ (podobno kot spremenljivka ~ polje), v katerega pišemo vrednosti
    });
    
    socket.on("srvBrisiBazo", function(){
		    console.log("Brisanje baze");
		    clientRedis.del("zapis", function (err, reply){
        });
    });

	socket.on("srvBeriIzBaze", function(){
		console.log("Beremo podatek iz baze: ");
		clientRedis.lrange("zapis", 0, -1, function (err, reply) { // preberemo iz baze, vsebina branja je v spremenljivki reply
			console.log(reply.toString());
			socket.emit("cliBeriBazo", reply); // brez .toString -> dobimo polje (Array) kot rezultat, kar nam omogoča, da dostopamo do posamezne vrednosti v polju, npr. številke, če vnašamo številke
		});
    });
	
});
var firmata = require('firmata');
var http = require('http').createServer(handler);
var fs = require('fs');
var io = require('socket.io').listen(http);

var redis = require("redis"),
    client = redis.createClient();
    
var d = new Date();

console.log("Connecting to redis...");
    
client.on("error", function (err) {
    console.log(err);
    process.exit(-1);
});


(function cleanup() {   //cleanup of redis lists

client.exists('analogReadVal', function(err, reply) {
    if (reply === 1) { //  
        console.log('Values list existed!');
        client.del('analogReadVal', function(err, reply) {
            if (err) {
                console.log(err);
            }
        });
    } 
});

client.exists('analogReadDate', function(err, reply) {
    if (reply === 1) { //  
        console.log('Dates list existed!');
        client.del('analogReadDate', function(err, reply) {
            if (err) {
                console.log(err);
            }
        });
    } 
});

})();

console.log("Started.");

function handler(request, response) {
    fs.readFile(__dirname + "/analog.html", function (err, data) {
        if (err) {
            response.writeHead(500, {"Content-Type": "text/plain"});
            return response.end("No html file found!");
        }
        response.writeHead(200);
        response.end(data);
    });
}

http.listen(8080);

var desiredValue = 0;

console.log("System ready.");

var board = new firmata.Board("/dev/ttyACM0", function() {
    console.log("Started Arduino...");
    console.log("Activating PIN A0");
    board.pinMode(0, board.MODES.ANALOG);
});    

board.on("ready", function() {
    
    console.log("Board is ready, starting");

// this code is continuous!    
    board.analogRead(0, function(value) {
        desiredValue = value; // set value to be sent to client
    });
    
    setInterval(saveValue, 50);
    
// this is to observe another client    
    io.sockets.on("connection", function(socket) {
        socket.emit("New client connected", "Event sent.");
        setInterval(sendValue, 40, socket);
        
        socket.on('message', function(msg) {
            console.log(msg);
        });
        
    });
    
    
});

function sendValue(socket) {
    socket.emit("ClientReadValue", {
       "desiredValue" : desiredValue 
    });
}

function saveValue() {
            client.rpush(['analogReadVal', desiredValue], function(err, reply) { // save the value to the list 
            if (err) { // print error
                console.log("Error with value list: " + err + ", reply: " + reply); 
            }
            else {  // remember the time the value was taken
                client.rpush(['analogReadDate', Date.now()], function(err, reply) {
                //console.log(value + " : " + Date.now()); //debug
                    if (err) { // print error and remove the value of this date
                        console.log("Error with dates list: " + err + ", reply: " + reply);
                        client.rpop(['analogReadVal']), function(err, reply) {
                            if (err) {
                                console.log("Something is terribly wrong with your redis access!");
                                process.exit(-1);
                            }
                        }
                    }
                });
            }
            
        });
}
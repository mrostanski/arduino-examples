var firmata = require('firmata');
var http = require('http').createServer(handler);
var fs = require('fs');
var io = require('socket.io').listen(http);


console.log("Started.");

function handler(request, response) {
    fs.readFile(__dirname + "/analog.html", function (err, data) {
        if (err) {
            response.writeHead(500, {"Content-Type": "text/plain"});
            return response.end("No html file found!");
        }
        response.writeHead(200);
        response.end(data);
    });
}

http.listen(8080);

var desiredValue = 0;

console.log("System ready.");

var board = new firmata.Board("/dev/ttyACM0", function() {
    console.log("Started Arduino...");
    console.log("Activating PIN A0");
    board.pinMode(0, board.MODES.ANALOG);
});    

board.on("ready", function() {
    
    console.log("Board is ready, starting");
    
    board.analogRead(0, function(value) {
        desiredValue = value;
    });
    
    io.sockets.on("connection", function(socket) {
        socket.emit("New client connected", "Event sent.");
    
        setInterval(sendValue, 40, socket);
        
    });
});

function sendValue(socket) {
    socket.emit("ClientReadValue", {
       "desiredValue" : desiredValue 
    });
}
